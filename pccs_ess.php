<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>PCCs</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="pcc.PNG" alt="PCC picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    You are displaying last 640 measurement sets recorded during ESS test. <br />
    <a href="pccs_ess_search.php">Search engine</a> | 
    <a href="pccs_ess_failures.php">Display all failures</a> | 
    <a href="pcc_ess_reports.php">Generate histograms</a> <br />
    
    </p>
<br/>

<?php

    $host    = "dbod-pcc-reliability-results.cern.ch:5500";
    $user    = "Francesca";
    $pass    = "Francesca.is.the.best!";
    $db_name = "reliability";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error();
        echo '<br/></status_error>';
    }
    else
    {
        $n_cards_1v2 = round(mysqli_fetch_array(mysqli_query($connection, "SELECT COUNT( DISTINCT pcc_barcode ) FROM ess WHERE status = 1 AND vouty < 1.8"))[0], 0);
        $n_cards_2v5 = round(mysqli_fetch_array(mysqli_query($connection, "SELECT COUNT( DISTINCT pcc_barcode ) FROM ess WHERE status = 1 AND vouty > 1.8"))[0], 0);

        echo '<p> So far ' .$n_cards_1v2. ' PCCs 1.2V out of 432 [' .(round((100*$n_cards_1v2/432), 1)). '%] and ' .$n_cards_2v5. ' PCCs 2.5V out of 432 [' .(round((100*$n_cards_2v5/432), 1)). '%] were positively screened for the detector installation.';
        //get results from database
        $result = mysqli_query($connection, "SELECT * FROM ess ORDER BY id DESC LIMIT 640");
        $all_property = array();  //declare an array for saving property

        //showing property
        echo '<table border = "1" align = "center">
                <tr >';  //initialize table tag
        while ($property = mysqli_fetch_field($result)) {
            echo '<td border="1">' . $property->name . '</td>';  //get field name for header
            $all_property[] = $property->name;  //save those to array
        }
        echo '</tr>'; //end tr tag

        //showing all data
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            foreach ($all_property as $item) {
                echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
            }
            echo '</tr>';
        }
        echo "</table></p>";
    }
?>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
