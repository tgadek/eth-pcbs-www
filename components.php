<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>Components database</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="components.png" alt="Components picture" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    <u>Barcode/QRcode encoding for the parts/components database:</u> <br /><br />
    for ECAL: <b>PEB-VFE/LVR/FE-[part]-[count]</b> <br />
    example: <b>PEB-VFE-CATIA-1234</b> -> Part for Ecal Barrel, VFE card, CATIA ASIC, 1234 pieces<br /><br />
    for BTL: <b>PBTL-PCC-[part]-[count]</b> <br />
    example: <b>PBTL-PCC-SHIELD-123</b> -> Part for Barrel Timing Layer, PCC card, shields, 123 pieces<br /><br /><br />
    for common parts of both sub-detectors: <b>PEBTL-[part]-[count]</b> <br /><br />
    <u>allowable parts:</u> <br />
     <b>NK</b> -> optical fibre NaKed fan-out <br /> 
     <b>FN</b> -> optical fibre rugged FaN-out and patch cords <br />  
     <b>SH</b> -> optical fibre SHuffle patch cords <br />  
     <b>TK</b> -> optical fibre TrunK cables <br />  
     <b>AC</b> -> optical fibre ACcessories (e.g. adaptors, pin, etc.) <br /> 
     <b>LCBL</b> -> Low voltage CaBLe<br />
     <b>BCBL</b> -> Bias voltage CaBLe<br />
     <b>LVPS</b> - Low Voltage Power Supply<br />
     <b>BVPS</b> - Bias Voltage Power Supply<br /><br />
     
    example: <b>PEBTL-LVPS-10</b> -> Part for Ecal Barrel or Barrel Timing Layer, Low Voltage Power Supply 10 pieces
    </p>
<br/>
<br/>
<h1>List of <b>components</b> inserted into the DB: </h1><br />
<?php

    $host    = "dbod-ecal-btl-construction.cern.ch:5501";
    $user    = "website";
    $pass    = "website_construction";
    $db_name = "components";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error();
        echo '<br/></status_error>';
    }
    else
    {
        //get results from database
        $result = mysqli_query($connection, "SELECT * FROM components");
        $all_property = array();  //declare an array for saving property

        //showing property
        echo '<table border = "1" align = "center">
                <tr >';  //initialize table tag
        while ($property = mysqli_fetch_field($result)) {
            echo '<td border="1">' . $property->name . '</td>';  //get field name for header
            $all_property[] = $property->name;  //save those to array
        }
        echo '</tr>'; //end tr tag

        //showing all data
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            foreach ($all_property as $item) {
                echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
            }
            echo '</tr>';
        }
        echo "</table>";
    }
?>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
