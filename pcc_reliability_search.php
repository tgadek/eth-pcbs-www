<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    h2 {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>

<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>PCCs</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="pcc.PNG" alt="PCC picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    You are displaying a search engine for single PCC reliability test data.<br /><br />
    </p>
<br/>

<h2 align="center">
<?php

    $host    = "dbod-pcc-reliability-results.cern.ch:5500";
    $user    = "website";
    $pass    = "website_PCCs";
    $db_name = "reliability";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error();
        echo '<br/></status_error>';
    }
    else
    {
        $sql = "SELECT pcc_id FROM results GROUP BY pcc_id"; // HAVING COUNT(pcc_id) = 1";
        $all_ids = mysqli_query($connection,$sql);

        $sql = "SELECT * FROM results WHERE id = 1";
        $all_parameters1 = mysqli_query($connection,$sql);
        $all_parameters2 = mysqli_query($connection,$sql);
        $all_parameters3 = mysqli_query($connection,$sql);
        $all_parameters4 = mysqli_query($connection,$sql);
        $all_parameters5 = mysqli_query($connection,$sql);
        $all_parameters6 = mysqli_query($connection,$sql);
    ?>

    <form method="POST">

        <label>Select PCC ID: </label><br />
        <select name="PCC_ID">
            <?php 
                // use a while loop to fetch data 
                // from the $all_categories variable 
                // and individually display as an option
                while ($pcc_ids = mysqli_fetch_array($all_ids, MYSQLI_ASSOC)):; 
            ?>
                <option value="<?php echo $pcc_ids["pcc_id"];
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_ids["pcc_id"];
                        // To show the ids
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <br>
        <label>Select parameters: </label><br />
        <select name="PARAMETER1">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters1)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <select name="PARAMETER2">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters2)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <select name="PARAMETER3">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters3)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <br />
        <select name="PARAMETER4">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters4)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <select name="PARAMETER5">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters5)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <select name="PARAMETER6">
            <?php 
                while ($pcc_parameters = mysqli_fetch_field($all_parameters6)):; 
            ?>
                <option value="<?php echo $pcc_parameters->name;
                    // The value of ID is the primary key
                ?>">
                    <?php echo $pcc_parameters->name
                        // To show the parameters names
                    ?>
                </option>
            <?php 
                endwhile; 
                // While loop must be terminated
            ?>
        </select>
        <br>
        <input type="submit" value="search" name="search">
    </form>

    
    <?php

        // The following code checks if the submit button is clicked 
        if(isset($_POST['search']))
        {
            
            $pcc_id = mysqli_real_escape_string($connection,$_POST['PCC_ID']);
            
            // Store the Category ID in a "id" variable
            $parameter1 = mysqli_real_escape_string($connection,$_POST['PARAMETER1']);
            $parameter2 = mysqli_real_escape_string($connection,$_POST['PARAMETER2']); 
            $parameter3 = mysqli_real_escape_string($connection,$_POST['PARAMETER3']);
            $parameter4 = mysqli_real_escape_string($connection,$_POST['PARAMETER4']);
            $parameter5 = mysqli_real_escape_string($connection,$_POST['PARAMETER5']); 
            $parameter6 = mysqli_real_escape_string($connection,$_POST['PARAMETER6']);   
            
            // Creating an insert query using SQL syntax and
            // storing it in a variable.
            $sql_query = "SELECT pcc_name, ".$parameter1.", ".$parameter2.", ".$parameter3.", ".$parameter4.", ".$parameter5.", ".$parameter6." FROM results WHERE pcc_id = '$pcc_id'";
            
            // The following code attempts to execute the SQL query
            // if the query executes with no errors 
            // a javascript alert message is displayed
            if($result = mysqli_query($connection,$sql_query))
            {
                echo '<script>alert("Data set is ready for a display.")</script>';
            }
        

            $all_property = array();  //declare an array for saving property
            echo "<br /><br />Data for PCC with ID = "; 
            echo $pcc_id;
            echo ", parameters: ";
            echo $parameter1;
            echo ", ";
            echo $parameter2;
            echo ", ";
            echo $parameter3;
            echo ", ";
            echo $parameter4;
            echo ", ";
            echo $parameter5;
            echo ", ";
            echo $parameter6;
            //showing property
            
            echo '<table border = "1" align = "center">
                    <tr >';  //initialize table tag
            while ($property = mysqli_fetch_field($result)) {
                echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                $all_property[] = $property->name;  //save those to array
            }
            echo '</tr>'; //end tr tag

            //showing all data
            while ($row = mysqli_fetch_array($result)) {
                echo "<tr>";
                foreach ($all_property as $item) {
                    echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                }
                echo '</tr>';
            }
            echo "</table>";
        }
    }
?>
</h2>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
