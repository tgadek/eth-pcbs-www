<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich VFE Test Setup</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
     h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    .center { display: block; text-align: center; margin-left: auto; font-family:"Helvetica", Helvetica, sans-serif; margin-right: auto; width:1000px; font-size:14px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>VFE Test Setup</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="vfe.PNG" alt="VFE picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <div class="center">
    <br />
      A photo of the VFE test setup:
      <br />
      <td><img src="pictures/vfe_test_setup.PNG" alt="VFE test setup picture" width="50%" height="50%" align="center"></td>
      <br /><br />
      A screenshot of the VFE test GUI:
      <br />
      <td><img src="pictures/vfe_test_setup_gui.png" alt="VFE test GUI picture" width="50%" height="50%" align="center"></td>
    </div>
</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
