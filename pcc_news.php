<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {border-collapse: collapse; border-radius: 10px; text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    td {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    th {font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>

</head>
<body>

<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>PCC News</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="pcc.PNG" alt="PCC picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2023-03-11">2023-03-11</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>32 PCCi are installed in a reliability demonstration setup. They are thermally cycled from -30 deg. C to 70 deg. C for 1245 cycles. This is to prove 0.995 reliability with 95% of confidence level for 20 years of operation in CMS MTD BTL conditions. The test will last until the end of August 2023.</td>
              </tr>
              <tr>
                <td><img src="pictures/pcci_reliability.png" alt="PCCi 3" width="50%" height="50%" align="center"></td>
              </tr>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2023-02-28">2023-02-28</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>80 PCCi cards underwent 200 passive thermal cycles from -35 deg. C to 80 deg. C, without developing further defects. 8 cards have been distributed to users in KSU and at CERN.</td>
              </tr>
              <tr>
                <td><img src="pictures/pccis_chamber.png" alt="PCCi 3 chamber" width="50%" height="50%" align="center"></td>
              </tr>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2023-02-21">2023-02-21</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>80 PCCi have been produced and tested. 5 cards had some defects, mostly problems with Pt1000 temperature sensors. 4 cards were repaired, 1 card is not regulating voltage in 1 DC-DC channel with an unknown source of failure.</td>
              </tr>
              <tr>
                <td><img src="pictures/pcci3.png" alt="PCCi 3" width="50%" height="50%" align="center"></td>
              </tr>
              </tr>
            </tbody>
          </table><br />


<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-17">2022-03-17</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>The datasheet of ETH 470 nH toroid is now available on <a href="https://cernbox.cern.ch/index.php/s/rgiRXBKaLaiNy3s">CERNBOX</a>.</td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-11">2022-03-11</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Company B sent a 3D model of toroid's bobbin for verfication. The model fits on the PCC and under the custom shield. We have given a green light for preparation of a mold and production of 650 toroids.</td>
              </tr>
              <tr>
                <td><img src="pictures/toroidB1.PNG" alt="Toroid 2" width="50%" height="50%" align="center"></td>
              </tr>
              </tr>
              <tr>
                <td><img src="pictures/toroidB2.png" alt="Toroid 2" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />
    
<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-07">2022-03-07</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Company A sent a 3D model of toroid's bobbin for verfication. The model fits on the PCC and under the custom shield. We have given a green light for preparation of a mold and production of 650 toroids.</td>
              </tr>
              <tr>
                <td><img src="pictures/toroidA1.PNG" alt="Toroid 1" width="50%" height="50%" align="center"></td>
              </tr>
              </tr>
              <tr>
                <td><img src="pictures/toroidA2.PNG" alt="Toroid 1" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <br />
    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-04">2022-03-04</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A set of near field emission measurements has been performed in order to evaluate shield's effectiveness. The plot below shows the emission of the new toroidal inductor, while being shielded and without shield. We conclude excellent effectiveness of the delivered shield prototype. Attenuation of 50 dBm has been achieved. Full measurements and comparisons are available on <a href="https://cernbox.cern.ch/index.php/s/jszvoce8dYoIG0S">CERNBOX</a>.</td>
              </tr>
              <tr>
                <td><img src="pictures/PCC_emission.png" alt="PCC emission" width="100%" height="100%" align="center"><br />
                Green - noise floor (electronics off), Red - unshielded emission, Blue - shielded emission
                </td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-03">2022-03-03</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A set of new prototype shields has been delivered by our contractor. The shields feature 300 um copper thickness and tin plating. One shield has been successfully and without problems soldered to a PCC cards. A set of measurements will be performed in order to evaluate shield's effectiveness.</td>
              </tr>
              <tr>
                <td><img src="pictures/PCC_mekoprint1.jpg" alt="PCC mekoprint" width="50%" height="50%" align="center"></td>
              </tr>
              <tr>
                <td><img src="pictures/PCC_mekoprint2.jpg" alt="PCC mekoprint" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center" id="2022-03-01">2022-03-01</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A new company entered the competition for the mass production of the LVR's toroids. An interesting quotatation and a proof of concept prototype have been both presented to ETH. We have decided to place an order for a batch of 650 pieces in parallel with the previous round's winner. This is to compare both products and have a better understanding where to place an order for the final 23,000 pieces production.</td>
              </tr>
              <tr>
                <td><img src="pictures/ita_toroid.jpg" alt="ITA toroid" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
