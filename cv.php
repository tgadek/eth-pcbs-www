<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tomasz Gadek - Curriculum Vitae</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 20px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
     .center { display: block; text-align: center; margin-left: auto; font-family:"Helvetica", Helvetica, sans-serif; margin-right: auto; width:1000px; font-size:14px;}
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="80%" align="center">
            <thead>
              <tr>
                <th><br />Curriculum Vitae<br /></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><br /><img src="pictures/CV_page1.png" alt="cv_page1 picture" width="100%" height="100%" align="center"></td>
              </tr>
              <tr>
                <td><br /><img src="pictures/CV_page2.png" alt="cv_page2 picture" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
        </table> 
        <br />
        <p>
        More on <a href="https://www.linkedin.com/in/tomasz-gadek-7845b170/">LinkedIn</a>
        <br /><br />
        Last Update - 18/08/2023
        </p>
    </h1>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
