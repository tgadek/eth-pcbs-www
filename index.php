<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    topic {text-align: center; font-family: Helvetica, sans-serif; font-size: 40px;}
    p {text-align: left;}
    div {text-align: right; font-family: Helvetica, sans-serif; font-size: 30px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0; table-layout: fixed;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px; table-layout: fixed;
      overflow:hidden;padding:0px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:30px; table-layout: fixed;
      font-weight:normal;overflow:hidden;padding:0px 5px;word-break:normal;}
    .tg .tg-vxqb{font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    img { max-width: 100%; height: auto;}
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<br />
<h1>
    <font-size="40"><br /> <a href="https://cms.cern/detector">CMS</a>   <br /> </font>
    <font-size="40"><br /> <a href="https://cms.cern/detector/measuring-energy/energy-electrons-and-photons-ecal">CMS - ECAL [VFE & LVR]  </a><br />   </font>
    <font-size="40"><a href="https://cms.cern/news/new-precision-timing-detector-cms-hl-lhc-upgrade">  CMS - MTD-BTL [PCC]</a>   <br /> <br /> </font>
    
        <table class="tg" width="50%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb">VFEs</th>
                  <th class="tg-vxqb">LVRs</th>
                  <th class="tg-vxqb">PCCs</th>                  
              </tr>
            </thead>
            <tbody>
              <tr>                  
                  <td class="tg-vxqb"><img src="vfe.PNG" alt="VFE picture" width="100%" height="100%" align="center"></td>
                  <td class="tg-vxqb"><img src="lvr.PNG" alt="LVR picture" width="100%" height="100%" align="center"></td>
                  <td class="tg-vxqb"><img src="pcc.PNG" alt="PCC picture" width="100%" height="100%" align="center"></td>                  
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="https://cernbox.cern.ch/s/1yBK2UbhRfaxC6L">Design</a></th>
                  <td class="tg-vxqb"><a href="https://cernbox.cern.ch/s/k1bM8nguqxBjZt4">Design</a></th>
                  <td class="tg-vxqb"><a href="https://cernbox.cern.ch/s/MAjr2i1HOp7V8dL">Design</a></th>
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="vfe_news.php">News</a></th>
                  <td class="tg-vxqb"><a href="lvr_news.php">News</a></th>
                  <td class="tg-vxqb"><a href="pcc_news.php">News</a></th>
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="vfe_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="lvr_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="pcc_setup.php">Test Setup</a></th>
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="vfes_full.php">Test Results DB</a></th>
                  <td class="tg-vxqb"><a href="lvrs_full.php">Test Results DB</a></th>
                  <td class="tg-vxqb"><a href="pccs_full.php">Test Results DB</a></th>
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="vfes_reliability.php">Reliability DB</a></th>
                  <td class="tg-vxqb"><a href="lvrs_reliability.php">Reliability DB</a></th>
                  <td class="tg-vxqb"><a href="pccs_reliability.php">Reliability DB</a></th>
              </tr>
              <tr>                  
                  <td class="tg-vxqb">ESS DB</th>
                  <td class="tg-vxqb">ESS DB</th>
                  <td class="tg-vxqb"><a href="pccs_ess.php">ESS DB</a></th>
              </tr>                 
              <tr>
                  <td class="tg-vxqb"><a href="vfe_pcbs.php">PCBs DB</a></th>
                  <td class="tg-vxqb"><a href="lvr_pcbs.php">PCBs DB</a></th>
                  <td class="tg-vxqb"><a href="pcc_pcbs.php">PCBs DB</a></th>
              </tr>  
              <tr>
                  <td class="tg-vxqb"><a href="components.php">Components DB</a></th>
                  <td class="tg-vxqb"><a href="components.php">Components DB</a></th>
                  <td class="tg-vxqb"><a href="components.php">Components DB</a></th>
              </tr> 
               <tr>
                  <td class="tg-vxqb"><a href="locations.php">Locations DB</a></th>
                  <td class="tg-vxqb"><a href="locations.php">Locations DB</a></th>
                  <td class="tg-vxqb"><a href="locations.php">Locations DB</a></th>
              </tr> 

                            
            </tbody>
        </table>
        <br />
        <a href="safir_structure.php">SAFIR</a>  
        <br />
        <br />

        <table class="tg" width="50%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb">SFCMs</th>
                  <th class="tg-vxqb">SFCDs</th>
                  <th class="tg-vxqb">SDIPs</th>
                  <th class="tg-vxqb">SP8</th>
                  <th class="tg-vxqb">SPOWs</th>                  
              </tr>
            </thead>
            <tbody>
            <tr>
                  <td class="tg-vxqb">(master, 4 outputs)</th>
                  <td class="tg-vxqb">(distribution, 6 outputs)</th>
                  <td class="tg-vxqb">(digital PETA interface)</th>
                  <td class="tg-vxqb">(PETA8 board)</th>
                  <td class="tg-vxqb">(16V -> 4.2V / 3.3V / 2.4V)</th>
              </tr>
              <tr>                  
                  <td class="tg-vxqb"><img src="safir_pictures/SFCM.JPG" alt="SFCM picture" width="100%" height="100%" align="center"></td>
                  <td class="tg-vxqb"><img src="safir_pictures/SFCD.JPG" alt="SFCD picture" width="100%" height="100%" align="center"></td>
                  <td class="tg-vxqb"><img src="safir_pictures/SDIP.png" alt="SDIP picture" width="100%" height="100%" align="center"></td>  
                  <td class="tg-vxqb"><img src="safir_pictures/SP6.JPG" alt="SP6 picture" width="100%" height="100%" align="center"></td>
                  <td class="tg-vxqb"><img src="safir_pictures/SPOW.JPG" alt="SPOW picture" width="100%" height="100%" align="center"></td>                
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="safir_test_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="safir_test_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="safir_test_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="safir_test_setup.php">Test Setup</a></th>
                  <td class="tg-vxqb"><a href="safir_test_setup.php">Test Setup</a></th>
              </tr>
              <tr>
                  <td class="tg-vxqb"><a href="sfcms_test_results.php">Test Results</a></th>
                  <td class="tg-vxqb"><a href="sfcds_test_results.php">Test Results</a></th>
                  <td class="tg-vxqb"><a href="sdips_test_results.php">Test Results</a></th>
                  <td class="tg-vxqb"><a href="sp8s_test_results.php">Test Results</a></th>
                  <td class="tg-vxqb"><a href="spows_test_results.php">Test Results</a></th>
              </tr>

                            
            </tbody>
        </table>
        <br />
        <a href="cv.php">My CV</a>  
        <br />
    </h1>
</body>
<footer align = "right">
    <foot> 
    <br /> powered by Tomasz Gadek & coffee <br />
    
    </foot>
</footer>
</html>
