<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {border-collapse: collapse; border-radius: 10px; text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    td {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    th {font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>VFE News</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="vfe.PNG" alt="VFE picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>  

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2023-09-13">2023-09-13</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>22 VFEs v3.2 with Lite-DTU 2.0 and production packaged CATIA 2.1 were designed, delivered and tested. The test results are available <a href="vfes_full.php">here</a> and the summary screenshots <a href="vfes_screenshots.php">here</a>.</td>
              </tr>
              <tr>
                <td><img src="pictures/22vfes.jpg" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2023-09-12">2023-09-12</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>6 VFEs with Lite-DTU 2.0 and SMA connectors to interface PLL outputs have been designed and produced. The boards will be used for various clock stability and shift studies.</td>
              </tr>
              <tr>
                <td><img src="pictures/vfe_pll.jpg" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2023-09-11">2023-09-11</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>60 unused Lite-DTUs were returned by the assembly company. These will be used in the first pass batch of the VFE production in 2024.</td>
              </tr>
              <tr>
                <td><img src="pictures/litedtus.jpg" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />


<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2023-05-02">2023-05-02</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>10 VFEs with Lite-DTU 2.0 and CATIA 2.1 have been assembled and tested. The cards were equipped with shields and installed in two places. 2 in TT for radiation test at CHARM, 8 in SM36. </td>
              </tr>
              <tr>
                <td><img src="pictures/vfe32catia21.png" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-08-22">2022-08-22</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>50 VFEs with Lite-DTU 2.0 and CATIA 2.0 have been assembled and tested. The cards were equipped with shields and installed in various places. 5 in TT in building 512, 5 in TT in building 13, 40 in SM36. </td>
              </tr>
              <tr>
                <td><img src="pictures/vfe32.png" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-04-22">2022-04-22</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>The timing performance has been measured for the channels on a VFE v3 card. The results clearly indicated that there is no difference among putting extra RC filter for the PLL bias. Remerkably powering form an LDO or from our DC-DC converters hosted  on the LVR card makes no difference in the timing performance of the ASICs. It is a big success of our DC-DC converters and LVR design.</td>
              </tr>
              <tr>
                <td><img src="pictures/VFE3-5_timing_vs_filter.png" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />    

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-04-25">2022-04-25</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A PLL locking problem was observed on the Lite-DTUs. We performed a study on the PLL locking, with a full scan of capacitors settings. The study was done on a population of 20 LiTE-DTUs. It seems that the locking is only achievable within quite a narrow range of voltages.</td>
              </tr>
              <tr>
                <td><img src="pictures/VFE_v3s_PLL_lock_vs_PLL_V_ref_plot.PNG" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
              <tr>
                <td><img src="pictures/VFE_v3s_PLL_lock_vs_PLL_V_ref_table.PNG" alt="VFE_v3" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-04-22">2022-04-22</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Six VFE v3 have been fully tested: optical inspection, signal quality and functional tests. Out of six VFEs v3 five are working perfectly fine. One cannot be configured, PLLs do not lock. The problems are investigated. <a href="https://cernbox.cern.ch/index.php/s/63qkZU4KPaJouHz">ALL TEST RESULTS ARE AVAILABLE ON CERNBOX.</a></td>
              </tr>
              <tr>
                <td><img src="pictures/vfe_v3.jpg" alt="VFE_v3" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-04-11">2022-04-11</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Six VFEs v3 have arrived to CERN, one was sent to our colleague from Paris Saclay for communication firmware development.</td>
              </tr>
              <tr>
                <td><img src="pictures/vfe_v3.jpg" alt="VFE_v3" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

<table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-18">2022-03-18</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Coated VFEs v2 are being cycled 100 times in a climatic chamber from -30 to +70 deg C.We are evaluating 3 types of coating material: transparent acrylic overcoat, acrylic protective laquer and 2-ingredient epoxy protective conformal coating solution. The microscopic images before and after cycling and mechanical scratch tests are available on <a href="https://cernbox.cern.ch/index.php/s/BVDuh2fuyHvZmyS">CERNBOX</a> </td>
              </tr>
              <tr>
                <td><img src="pictures/coated_cycling.JPG" alt="Elfab Shipment" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-07">2022-03-07</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>3 different types of conformal coating materials will be tested in order to provide an optimal coating for the HV blocking capacitors on the VFE card. We have purchased: transparent acrylic overcoat, acrylic protective laquer and 2-ingredient epoxy protective conformal coating solution. Coated VFEs v2 will be cycled 100 times in a climatic chamber from -30 to +70 deg C. The microscopic images before and after cycling and mechanical scratch tests will be included in the study.</td>
              </tr>
              <tr>
                <td><img src="pictures/conformal_coating.jpg" alt="conformal_coating" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-14">2022-03-14</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Tektronix AFG31252 arbitrary waveform generator has been characterized for the APD pulse emulation capabilities. The device was measured over 5 days of constant running with both channels emitting pulses of 25 ns width, 50 mV aplitude and with 100 kHz frequency. The measured energy of the pulse is 1.23 pJ. Estimated jitter pk-pk is smaller than 75ps. Oscillograms with measured parameters of the pulse shape, its rising and falling edge below.</td>
              </tr>
              <tr>
                <td><img src="pictures/AFG31252_pulse.jpg" alt="AFG31252_pulse" width="100%" height="100%" align="center"></td>
              </tr>
              <tr>
                <td><img src="pictures/AFG31252_pulse_re.jpg" alt="AFG31252_pulse" width="100%" height="100%" align="center"></td>
              </tr>
              <tr>
                <td><img src="pictures/AFG31252_pulse_fe.jpg" alt="AFG31252_pulse" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-07">2022-03-07</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A set of 6 PCBs, ASICs and connectors has been shipped to the assembly company. The assembled boards are expected to return to us in 2-3 weeks time.</td>
              </tr>
              <tr>
                <td><img src="pictures/elfab_shipment_vfev3.JPG" alt="Elfab Shipment" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

    <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-01">2022-03-01</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A set of 32 LiTE-DTUs has arrived to CERN. Together with CATIAs (540 pieces), set of connectors and produced PCB panels will be shipped to the assembly company before the end of the week.</td>
              </tr>
              <tr>
                <td><img src="pictures/32lite_dtus.JPG" alt="LiTE_DTUs" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center">2022-02-28</th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A new signal generator has joined the ETH fleet as a new VFE calibration signal source. The Tektronix AFG31252 is a 14-bit 2-channel 250MHz 2GSps Arbitrary Waveform Generator. </td>
              </tr>
              <tr>
                <td><img src="pictures/signal_generator.jpg" alt="AFG31252" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-25">2022-02-25</a></th>
              </tr>
            </thead>
            <tbody>             
              <tr>
                <td>Metal housings have been produced for VFEv3 cards and successfully integrated. The test in a trigger tower showed a perfect alignment of respective pins and screws.</td>
              </tr>
              <tr>
                <td><img src="pictures/vfe_housing1.jpg" alt="VFE housing1" width="50%" height="50%" align="center">
                <img src="pictures/vfe_housing2.jpg" alt="VFE housing2" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-23">2022-02-23</a></th>
              </tr>
            </thead>
            <tbody>             
              <tr>
                <td>4 panels of 2 (8 PCBs) VFEv3 cards arrived to CERN</td>
              </tr>
              <tr>
                <td><img src="pictures/vfe_panel.JPG" alt="VFE panel" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
        </table><br />

        <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-01">2022-02-01</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A set of 540 CATIAs has arrived to CERN to Saclay. Together with LiTE-DTUs, a set of connectors and produced PCB panels will be shipped to the assembly for a pilot assembly of 6 VFEv3 PCBs.</td>
              </tr>
              <tr>
                <td><img src="pictures/catia.jpg" alt="CATIAs" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
