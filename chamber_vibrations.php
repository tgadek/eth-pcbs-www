<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 20px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
     .center { display: block; text-align: center; margin-left: auto; font-family:"Helvetica", Helvetica, sans-serif; margin-right: auto; width:1000px; font-size:14px;}
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="80%" align="center">
            <thead>
              <tr>
                <th><br />Climatic Chamber Vibrations Measurements<br /></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><br /><img src="pictures/vibrations_setup.PNG" alt="vibrations_setup picture" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
        </table> 
        <br />
    </h1>
    <div class="center">
      <p>4 plots of data taken by the 3-axis vibration and temperature sensors mounted on top of PCC cards:
      <br /><br />
      <td><img src="pictures/vibration_plots001.png" alt="vibration_plots001 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots002.png" alt="vibration_plots002 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots003.png" alt="vibration_plots003 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots004.png" alt="vibration_plots004 picture" width="100%" height="100%" align="center"></td>
      <br /><br /><br />
      Initial 4 sensors did not last even 3 thermal cycles in the chamber. They were replaced with a new set of 4 sensors, that again failed after 2 thermal cycles. The sensors are rated from -30 to +80 deg. C, the failures were communicated to the producer as a reclamation case.
      <br /><br />
      <td><img src="pictures/vibration_plots1001.png" alt="vibration_plots1001 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots1002.png" alt="vibration_plots002 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots1003.png" alt="vibration_plots003 picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      <td><img src="pictures/vibration_plots1004.png" alt="vibration_plots004 picture" width="100%" height="100%" align="center"></td>
      <br /><br /><br />
      Additionally an external single axis vibration sensor has been mounted on the chassis of the chamber to measure its vibrations in terms of amplitude of displacement, vibration speed and vibration acceleration expressed in two units [g and m/s^2].
      <br /><br />
      Displacement plot - full data:
      <td><img src="pictures/displacement_plot.png" alt="displacement_plot picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      Displacement plot reduced to 5 sigma, where points with values above 5 sigma are assigned with 5 sigma value.
      <td><img src="pictures/displacement_plot_5sigma.png" alt="displacement_plot_5sigma picture" width="100%" height="100%" align="center"></td>
      <br />
      <br />
      Velocity plot - full data:
      <td><img src="pictures/velocity_plot.png" alt="velocity_plot picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      Velocity plot reduced to 5 sigma, where points with values above 5 sigma are assigned with 5 sigma value.
      <td><img src="pictures/velocity_plot_5sigma.png" alt="velocity_plot_5sigma picture" width="100%" height="100%" align="center"></td>
      <br />
      <br />
      Acceleration plot in g unit - full data:
      <td><img src="pictures/acceleration_g_plot.png" alt="acceleration_g_plot picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      Acceleration plot reduced to 5 sigma, where points with values above 5 sigma are assigned with 5 sigma value.
      <td><img src="pictures/acceleration_g_plot_5sigma.png" alt="acceleration_g_plot_5sigma picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      Acceleration plot in Si unit - full data:
      <td><img src="pictures/acceleration_m_plot.png" alt="acceleration_m_plot picture" width="100%" height="100%" align="center"></td>
      <br /><br />
      Acceleration plot reduced to 5 sigma, where points with values above 5 sigma are assigned with 5 sigma value.
      <td><img src="pictures/acceleration_m_plot_5sigma.png" alt="acceleration_m_plot_5sigma picture" width="100%" height="100%" align="center"></td>
      
      <br /><br />
      </p>
    </div>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
