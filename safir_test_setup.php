<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; max-width: 60%; margin: auto; display: block; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>SAFIR TEST SETUP</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="safir_pictures/SDIP.png" alt="SDIP picture" width="100%" height="100%" align="center"></td>
              </tr>
            </tbody>
        </table>
        <br />
        </h1>
        <p> SPOW emulation card was constructed to test the SFCM cards: it features an input PI filter 47uF (25 V) - 82 nF (6A) - 47 uF (25 V), a 3.3 V output LDO. Can be powered at 5 V via LEMO cable, screw terminal or USB. It has 3 LEDs for input voltage and output voltage status monitoring and an overcurrent protection. As an option the output is available via an SMB connector to measure the output's noise. <br /> <br />
    <img src="safir_pictures/fake_spow.jpg" alt="fake SPOW picture" width="60%" height="60%" align="center">
</p>
</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
