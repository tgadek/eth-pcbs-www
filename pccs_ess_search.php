<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    h2 {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>

<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>PCCs</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="pcc.PNG" alt="PCC picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    You are displaying a search engine for a single PCC ESS data.
    </p>
<br/>

<h2 align="center">
<?php

    $host    = "dbod-pcc-reliability-results.cern.ch:5500";
    $user    = "website";
    $pass    = "website_PCCs";
    $db_name = "reliability";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error();
        echo '<br/></status_error>';
    }
    else
    {
        $sql = "SELECT pcc_id FROM ess GROUP BY pcc_id"; // HAVING COUNT(pcc_id) = 1";
        $all_ids = mysqli_query($connection,$sql);

        
    ?>

    <form method="POST">

        <label>PCC barcode label: </label>
        <input type='text' name='barcode'>            
        <input type="submit" value="search" name="search">
    </form>
    </h2>
    <p>
    <?php

        // The following code checks if the submit button is clicked 
        if(isset($_POST['search']))
        {
            // Creating an insert query using SQL syntax and
            // storing it in a variable.
            $barcode = $_POST['barcode'];
            $sql_query = "SELECT * FROM ess WHERE pcc_barcode = '$barcode'";
            
            // The following code attempts to execute the SQL query
            // if the query executes with no errors 
            // a javascript alert message is displayed
            if($result = mysqli_query($connection,$sql_query))
            {
                echo '<script>alert("Data set is ready for a display.")</script>';
            }
        

            $all_property = array();  //declare an array for saving property
            echo "<br /><br />Data for PCC with barcode = "; 
            echo $barcode;
            
            
            echo '<table border = "1" align = "center">
                    <tr >';  //initialize table tag
            while ($property = mysqli_fetch_field($result)) {
                echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                $all_property[] = $property->name;  //save those to array
            }
            echo '</tr>'; //end tr tag

            //showing all data
            while ($row = mysqli_fetch_array($result)) {
                echo "<tr>";
                foreach ($all_property as $item) {
                    echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                }
                echo '</tr>';
            }
            echo "</table>";
        }
    }
?>
</p>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
