<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
     .center { display: block; text-align: center; margin-left: auto; font-family:"Helvetica", Helvetica, sans-serif; margin-right: auto; width:1000px; font-size:14px;}
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>LVR Test Setup</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="lvr.PNG" alt="LVR picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>

    <br />
    
    </h1>
    <div class="center">
    <br />
      A photo of the LVR test setup:
      <br />
      <td><img src="pictures/lvr_test_setup.png" alt="LVR test setup picture" width="50%" height="50%" align="center"></td>
      <br /><br />
      A screenshot of the LVR test GUI:
      <br />
      <td><img src="pictures/lvr_test_setup_gui.png" alt="LVR test GUI picture" width="50%" height="50%" align="center"></td>
    </div>
</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
