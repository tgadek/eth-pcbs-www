<?php
    use \koolreport\widgets\google\Histogram;

    $host    = "dbod-pcc-reliability-results.cern.ch:5500";
    $user    = "website";
    $pass    = "website_PCCs";
    $db_name = "reliability";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error();
        echo '<br/></status_error>';
    }
    else
    {
            //get results from database
            $result = mysqli_query($connection, "SELECT pcc_id, voutx, vouty, voutz, chamber_temp FROM ess WHERE status = 1");
            $dataX = $dataY12 = $dataY25 = $dataZ = $dataXcold = $dataY12cold = $dataY25cold = $dataZcold = array(["pcc_id", "vout"]);

            while(($row =  mysqli_fetch_array($result))) {
                if ($row['chamber_temp'] < 0.0 )
                {
                    $dataXcold[] = [$row['pcc_id'], $row['voutx']];
                    if ($row['vouty'] < 2.0 ) $dataY12cold[] = [$row['pcc_id'], $row['vouty']];
                    else $dataY25cold[] = [$row['pcc_id'], $row['vouty']];
                    $dataZcold[] = [$row['pcc_id'], $row['voutz']];
                }
                else
                {
                    $dataX[] = [$row['pcc_id'], $row['voutx']];
                    if ($row['vouty'] < 2.0 ) $dataY12[] = [$row['pcc_id'], $row['vouty']];
                    else $dataY25[] = [$row['pcc_id'], $row['vouty']];
                    $dataZ[] = [$row['pcc_id'], $row['voutz']];
                }
            }

        
    } 
    
?>
<div class="report-content">
    <div class="text-center">
        <h1>PCC Histograms</h1>
        <p class="lead">
            Here are the histograms of the output voltage from different channels of the PCCs.
        </p>
    </div>
</div>   
    <div class="row">
        <div class="column">
        
            <?php

            function display_stats($connection, $column_name, $temperature, $extra_query_condition){
                $mean = 0;
                if ($temperature > 0){
                    $mean = round(mysqli_fetch_array(mysqli_query($connection, "SELECT AVG($column_name) FROM ess WHERE status = 1 AND chamber_temp > 0 $extra_query_condition"))[0], 3);
                    $min = round(mysqli_fetch_array(mysqli_query($connection, "SELECT MIN($column_name) FROM ess WHERE status = 1 AND chamber_temp > 0 $extra_query_condition"))[0], 3);
                    $max = round(mysqli_fetch_array(mysqli_query($connection, "SELECT MAX($column_name) FROM ess WHERE status = 1 AND chamber_temp > 0 $extra_query_condition"))[0], 3);
                    $sd = round(mysqli_fetch_array(mysqli_query($connection, "SELECT STDDEV($column_name) FROM ess WHERE status = 1 AND chamber_temp > 0 $extra_query_condition"))[0], 3);
                    echo '<p2> Mean: ', $mean;
                    echo ' &nbsp;SD: ', $sd, ' (' .round((100*$sd/$mean), 2). '%)';
                    echo '  &nbsp;Min: ', $min;
                    echo '  &nbsp;Max: ', $max;
                    echo '  &nbsp;Entries: ', round(mysqli_fetch_array(mysqli_query($connection, "SELECT COUNT($column_name) FROM ess WHERE status = 1 AND chamber_temp > 0 $extra_query_condition"))[0], 3), ' <br/>';
                    echo 'Overall population boundaries: ', $mean, ' V <sup>+', round(100*($max-$mean)/$mean, 3), '%</sup><sub>', round(100*($min-$mean)/$mean, 3), '% </sub>';
                    echo '<br/></p2>';
                }
                else{
                    $mean = round(mysqli_fetch_array(mysqli_query($connection, "SELECT AVG($column_name) FROM ess WHERE status = 1 AND chamber_temp < 0 $extra_query_condition"))[0], 3);
                    $min = round(mysqli_fetch_array(mysqli_query($connection, "SELECT MIN($column_name) FROM ess WHERE status = 1 AND chamber_temp < 0 $extra_query_condition"))[0], 3);
                    $max = round(mysqli_fetch_array(mysqli_query($connection, "SELECT MAX($column_name) FROM ess WHERE status = 1 AND chamber_temp < 0 $extra_query_condition"))[0], 3);
                    $sd = round(mysqli_fetch_array(mysqli_query($connection, "SELECT STDDEV($column_name) FROM ess WHERE status = 1 AND chamber_temp < 0 $extra_query_condition"))[0], 3);
                    echo '<p2> Mean: ', $mean;
                    echo ' &nbsp;SD: ', $sd, ' (' .round((100*$sd/$mean), 2). '%)';
                    echo '  &nbsp;Min: ', $min;
                    echo '  &nbsp;Max: ', $max;
                    echo '  &nbsp;Entries: ', round(mysqli_fetch_array(mysqli_query($connection, "SELECT COUNT($column_name) FROM ess WHERE status = 1 AND chamber_temp < 0 $extra_query_condition"))[0], 3), ' <br/>';
                    echo 'Overall population boundaries: ', $mean, ' V <sup>+', round(100*($max-$mean)/$mean, 3), '%</sup><sub>', round(100*($min-$mean)/$mean, 3), '% </sub>';
                    echo '<br/></p2>';
                }
                echo "<br/>";
                return $mean;
            }


            $bucketSizeY25 = 0.01;
            $bucketSizeY12 = 0.008;
            $bucketSizeXZ = 0.01;
            $maxNumBuckets = 12;
                Histogram::create(array(
                    "title"=>"PCC output voltage channel X, at +70°C",
                    "options" => array(
                        "histogram" => array( "bucketSize"=>$bucketSizeXZ ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),
                    "dataSource"=>$dataX
                ));
                $mean_voutx_p70 = display_stats($connection, "voutx", 70, "");

                Histogram::create(array(
                    "title"=>"PCC output voltage channel Y (1.2 V variant), at +70°C",
                    "colorScheme"=>"green",
                    "options" => array(
                        "colors" => ['navy'],
                        "histogram" => array( "bucketSize"=>$bucketSizeY12 ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),
                    "dataSource"=>$dataY12
                ));
                $mean_vouty12_p70 = display_stats($connection, "vouty", 70, " AND vouty < 1.8");
                
                Histogram::create(array(
                    "title"=>"PCC output voltage channel Y (2.5 V variant),  at +70°C",
                    "options" => array(
                        "colors" => ['indigo'],
                        "histogram" => array( "bucketSize"=>$bucketSizeY25 ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),                    
                    "dataSource"=>$dataY25
                ));
                $mean_vouty25_p70 = display_stats($connection, "vouty", 70, " AND vouty > 1.8");

                Histogram::create(array(
                    "title"=>"PCC output voltage channel Z, at +70°C",
                    "options" => array(
                        "histogram" => array( "bucketSize"=>$bucketSizeXZ ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),    
                    "dataSource"=>$dataZ              
                    
                ));
                $mean_voutz_p70 = display_stats($connection, "voutz", 70, "");
                
                $distribution_cut_threshold_12 = 0.03;
                $distribution_cut_threshold_18 = 0.05;
                $distribution_cut_threshold_25 = 0.04;
                $upper_bound_multiplier_12 = 1 + $distribution_cut_threshold_12;
                $lower_bound_multiplier_12 = 1 - $distribution_cut_threshold_12;
                $upper_bound_multiplier_18 = 1 + $distribution_cut_threshold_18;
                $lower_bound_multiplier_18 = 1 - $distribution_cut_threshold_18;
                $upper_bound_multiplier_25 = 1 + $distribution_cut_threshold_25;
                $lower_bound_multiplier_25 = 1 - $distribution_cut_threshold_25; 

                 $sql_query = "SELECT DISTINCT pcc_barcode FROM ess WHERE
                status = 1 AND chamber_temp > 0 AND 
                ( 
                voutx > ".($upper_bound_multiplier_18 * $mean_voutx_p70)." 
                OR voutx < ".($lower_bound_multiplier_18 * $mean_voutx_p70)." 
                OR (vouty < 1.8 AND vouty > ".($upper_bound_multiplier_12 * $mean_vouty12_p70).") 
                OR (vouty < 1.8 AND vouty < ".($lower_bound_multiplier_12 * $mean_vouty12_p70).") 
                OR (vouty > 1.8 AND vouty > ".($upper_bound_multiplier_25 * $mean_vouty25_p70).") 
                OR (vouty > 1.8 AND vouty < ".($lower_bound_multiplier_25 * $mean_vouty25_p70).")
                OR voutz > ".($upper_bound_multiplier_18 * $mean_voutz_p70)." 
                OR voutz < ".($lower_bound_multiplier_18 * $mean_voutz_p70)." 
                ) ORDER BY pcc_barcode DESC";
                        

                $result = mysqli_query($connection, $sql_query);   
                $all_property = array();  //declare an array for saving property
                
                //echo "<p2><br /><br />Outliers at ", ($distribution_cut_threshold_18 * 100), "% away from the mean value at +70°C"; 
                echo "<p2><br /><br />Outliers at the load dependent threshold (".($distribution_cut_threshold_12*100).", ".($distribution_cut_threshold_25*100)." and ".($distribution_cut_threshold_18*100)."%) away from the mean value at +70°C"; 
                
                echo '<table border = "1" align = "center">
                        <tr >';  //initialize table tag
                while ($property = mysqli_fetch_field($result)) {
                    echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                    $all_property[] = $property->name;  //save those to array
                }
                echo '</tr>'; //end tr tag

                //showing all data
                $counter = 0;
                while ($row = mysqli_fetch_array($result)) {
                    $counter++;
                    echo "<tr>";
                    foreach ($all_property as $item) {
                        echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                    }
                    echo '</tr>';
                }
                echo "</table>";
                echo 'Total entries: ', $counter;
                echo '</p2> <br/>';

                $sql_query = "SELECT pcc_barcode, date, voutx, vouty, voutz  FROM ess WHERE
                status = 1 AND chamber_temp > 0 AND 
                ( 
                voutx > ".($upper_bound_multiplier_18 * $mean_voutx_p70)." 
                OR voutx < ".($lower_bound_multiplier_18 * $mean_voutx_p70)." 
                OR (vouty < 1.8 AND vouty > ".($upper_bound_multiplier_12 * $mean_vouty12_p70).") 
                OR (vouty < 1.8 AND vouty < ".($lower_bound_multiplier_12 * $mean_vouty12_p70).") 
                OR (vouty > 1.8 AND vouty > ".($upper_bound_multiplier_25 * $mean_vouty25_p70).") 
                OR (vouty > 1.8 AND vouty < ".($lower_bound_multiplier_25 * $mean_vouty25_p70).")
                OR voutz > ".($upper_bound_multiplier_18 * $mean_voutz_p70)." 
                OR voutz < ".($lower_bound_multiplier_18 * $mean_voutz_p70)." 
                ) ORDER BY pcc_barcode DESC";
                        

                $result = mysqli_query($connection, $sql_query);   
                $all_property = array();  //declare an array for saving property
                
                echo "<p2>Details of the threshold crossing:"; 
                
                echo '<table border = "1" align = "center">
                        <tr >';  //initialize table tag
                while ($property = mysqli_fetch_field($result)) {
                    echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                    $all_property[] = $property->name;  //save those to array
                }
                echo '<td border="1">mean_x</td>';
                echo '<td border="1">mean_y12</td>';
                echo '<td border="1">mean_y25</td>';
                echo '<td border="1">mean_z</td>';
                echo '</tr>'; //end tr tag

                //showing all data
                $counter = 0;
                while ($row = mysqli_fetch_array($result)) {
                    $counter++;
                    echo "<tr>";
                    foreach ($all_property as $item) {
                        echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                    }
                    echo '<td border="1">' .$mean_voutx_p70. '</td>';
                    echo '<td border="1">' .$mean_vouty12_p70. '</td>';
                    echo '<td border="1">' .$mean_vouty25_p70. '</td>';
                    echo '<td border="1">' .$mean_voutz_p70. '</td>';
                    echo '</tr>';
                }
                echo "</table>";
                echo '</p2>';
            ?>
       
        </div>
        <div class="column">
       
        <?php
            $bucketSizeY25 = 0.017;
            $bucketSizeY12 = 0.008;
            $bucketSizeXZ = 0.01;
            $maxNumBuckets = 12;
            Histogram::create(array(
                "title"=>"PCC output voltage channel X, at -30°C",
                "options" => array(
                        "histogram" => array( "bucketSize"=>$bucketSizeXZ, "maxNumBuckets" => $maxNumBuckets ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),
                "dataSource"=>$dataXcold
            ));
            $mean_voutx_m30 = display_stats($connection, "voutx", -30, "");
            
            Histogram::create(array(
                "title"=>"PCC output voltage channel Y (1.2 V variant), at -30°C",
                "options" => array(
                        "colors" => ['navy'],
                        "histogram" => array( "numBucketsRule"=>'rice' ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),  
                "dataSource"=>$dataY12cold
            ));
            $mean_vouty12_m30 = display_stats($connection, "vouty", -30, " AND vouty < 1.8");

            
            Histogram::create(array(
                "title"=>"PCC output voltage channel Y (2.5 V variant),  at -30°C",
                "options" => array(
                        "colors" => ['indigo'],
                        "histogram" => array( "bucketSize"=>$bucketSizeY25, "maxNumBuckets" => $maxNumBuckets ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),  
                "dataSource"=>$dataY25cold
            ));
            $mean_vouty25_m30 = display_stats($connection, "vouty", -30, " AND vouty > 1.8");

            Histogram::create(array(
                "title"=>"PCC output voltage channel Z, at -30°C",
                "options" => array(
                        "histogram" => array( "bucketSize"=>$bucketSizeXZ, "maxNumBuckets" => $maxNumBuckets ),
                        "vAxis" => [
                            "title" => 'N_measurements [1/cycle/converter]',
                        ],
                        "hAxis" => [
                            "title" => 'Voltage [V]'
                        ]
                    ),
                "dataSource"=>$dataZcold
            ));
            $mean_voutz_m30 = display_stats($connection, "voutz", -30, "");

            $sql_query = "SELECT DISTINCT pcc_barcode FROM ess WHERE
            status = 1 AND chamber_temp < 0 AND 
            ( 
            voutx > ".($upper_bound_multiplier_18 * $mean_voutx_m30)." 
            OR voutx < ".($lower_bound_multiplier_18 * $mean_voutx_m30)." 
            OR (vouty < 1.8 AND vouty > ".($upper_bound_multiplier_12 * $mean_vouty12_m30).") 
            OR (vouty < 1.8 AND vouty < ".($lower_bound_multiplier_12 * $mean_vouty12_m30).") 
            OR (vouty > 1.8 AND vouty > ".($upper_bound_multiplier_25 * $mean_vouty25_m30).") 
            OR (vouty > 1.8 AND vouty < ".($lower_bound_multiplier_25 * $mean_vouty25_m30).")
            OR voutz > ".($upper_bound_multiplier_18 * $mean_voutz_m30)." 
            OR voutz < ".($lower_bound_multiplier_18 * $mean_voutz_m30)." 
            ) ORDER BY pcc_barcode DESC";
                    
           
            $result = mysqli_query($connection, $sql_query);   
            $all_property = array();  //declare an array for saving property
            
            //echo "<p2><br /><br />Outliers at ", ($distribution_cut_threshold_18 * 100) , "% away from the mean value at -30°C"; 
            echo "<p2><br /><br />Outliers at the load dependent threshold (".($distribution_cut_threshold_12*100).", ".($distribution_cut_threshold_25*100)." and ".($distribution_cut_threshold_18*100)."%) away from the mean value at -30°C";

            echo '<table border = "1" align = "center">
                    <tr >';  //initialize table tag
            while ($property = mysqli_fetch_field($result)) {
                echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                $all_property[] = $property->name;  //save those to array
            }
            echo '</tr>'; //end tr tag

            //showing all data
            $counter = 0;
            while ($row = mysqli_fetch_array($result)) {
                $counter++;
                echo "<tr>";
                foreach ($all_property as $item) {
                    echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                }
                echo '</tr>';
            }            
            echo "</table>";
            echo 'Total entries: ', $counter;
            echo '</p2> <br/>';
  
            $sql_query = "SELECT pcc_barcode, date, voutx, vouty, voutz  FROM ess WHERE
            status = 1 AND chamber_temp < 0 AND 
            ( 
            voutx > ".($upper_bound_multiplier_18 * $mean_voutx_m30)." 
            OR voutx < ".($lower_bound_multiplier_18 * $mean_voutx_m30)." 
            OR (vouty < 1.8 AND vouty > ".($upper_bound_multiplier_12 * $mean_vouty12_m30).") 
            OR (vouty < 1.8 AND vouty < ".($lower_bound_multiplier_12 * $mean_vouty12_m30).") 
            OR (vouty > 1.8 AND vouty > ".($upper_bound_multiplier_25 * $mean_vouty25_m30).") 
            OR (vouty > 1.8 AND vouty < ".($lower_bound_multiplier_25 * $mean_vouty25_m30).")
            OR voutz > ".($upper_bound_multiplier_18 * $mean_voutz_m30)." 
            OR voutz < ".($lower_bound_multiplier_18 * $mean_voutz_m30)." 
            ) ORDER BY pcc_barcode DESC";
                    
           
            $result = mysqli_query($connection, $sql_query);   
            $all_property = array();  //declare an array for saving property
            
            echo "<p2>Details of the threshold crossing:"; 
            
            echo '<table border = "1" align = "center">
                    <tr >';  //initialize table tag
            while ($property = mysqli_fetch_field($result)) {
                echo '<td border="1">' . $property->name . '</td>';  //get field name for header
                $all_property[] = $property->name;  //save those to array
            }
            echo '<td border="1">mean_x</td>';
            echo '<td border="1">mean_y12</td>';
            echo '<td border="1">mean_y25</td>';
            echo '<td border="1">mean_z</td>';
            echo '</tr>'; //end tr tag

            //showing all data
            
            while ($row = mysqli_fetch_array($result)) {
                
                echo "<tr>";
                foreach ($all_property as $item) {
                    echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
                }
                echo '<td border="1">' .$mean_voutx_m30. '</td>';
                echo '<td border="1">' .$mean_vouty12_m30. '</td>';
                echo '<td border="1">' .$mean_vouty25_m30. '</td>';
                echo '<td border="1">' .$mean_voutz_m30. '</td>';
                echo '</tr>';
            }            
            echo "</table>";
            echo '</p2>';                
        ?>       
        
        </div>
    </div>
