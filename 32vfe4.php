<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>VFEs</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="vfe.PNG" alt="VFE picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    Summary test results of the 32 VFE v4.0 cards with LiTE-DTU v3.0.
</p>
<br/>

<?php

    $host    = "dbod-vfe-test-results.cern.ch:5506";
    $user    = "website";
    $pass    = "website_VFEs";
    $db_name = "vfe_temp_test_results";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error(); 
        echo '<br/></status_error>';
    }
    else
    {
        //get results from database
        $result = mysqli_query($connection, "SELECT vfe_id, date, screenshot FROM results WHERE vfe_id<40 AND id>740 ORDER BY vfe_id  LIMIT 100");



        $all_property = array();  //declare an array for saving property

        //showing property
        echo '<table border = "1" align = "center">
                <tr >';  //initialize table tag
        while ($property = mysqli_fetch_field($result)) {
            echo '<td border="1">' . $property->name . '</td>';  //get field name for header
            $all_property[] = $property->name;  //save those to array
        }
        echo '</tr>'; //end tr tag

        //showing all data
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            
            echo '<td border="1">' . $row['vfe_id'] . '</td> <td>' . $row['date'] . '</td> <td>';?>
            

                <?php #while($row = $result->fetch_assoc()){ ?>
                    <img src="data:image/jpg;charset=utf8;base64,
                    <?php echo base64_encode($row['screenshot']); ?> ">
                <?php #} 
            
            echo '</td>'; //get items using property value

            echo '</tr>';
        }
        echo "</table>";
    }
?>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
