<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {border-collapse: collapse; border-radius: 10px; text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    td {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    th {font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th align="center">LVR News</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="lvr.PNG" alt="LVR picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
 <br />

 <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2023-06-09">2023-06-09</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>75 LVR cards version 3.2 have been produced and tested. These cards host a reduced to 4 number of DC-DC converters and an additional linear regulator to power GBT-SCA present on the FE card. 8 cards are installed in SM36, 17 cards were shipped to the collaborators in Belgrade, Serbia for the VFE+LVR burn-in setup development.</td>
              </tr>
              <tr>
                <td><img src="pictures/lvr32.png" alt="LVR 3.2" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

 <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-04-26">2022-04-26</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>Because of the low power consumption of the VFE cards we might consider moving to 4 DC-DC converters instead of 6. The converters powering 5 VFEs would be loaded as following: 
                <br /> 2.13 A for the 2.5 V regulating converter
                <br /> 3.11 A for the 1.2 V regulating converter
                <br /> We remind that it is possible to load the converters with a continuos current of 4 A.
                
              </td>
              </tr>
            </tbody>
          </table><br />

 <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-17">2022-03-17</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>The datasheet of ETH 470 nH toroid is now available on <a href="https://cernbox.cern.ch/index.php/s/rgiRXBKaLaiNy3s">CERNBOX</a>.</td>
              </tr>
            </tbody>
          </table><br />

 <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-08">2022-03-08</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A dummy LVR v3.1 card has been delivered to our colleagues from University of Notre Dame US. The card will distribute powering and help in the new FE card evaluation.</td>
              </tr>
              <tr>
                <td><img src="pictures/lvr_dummy.png" alt="LVR dummy" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

 <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-04">2022-03-04</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td> New prototype shields have been delivered by our contractor. The shields feature 300 um copper thickness and tin plating.</td>                
              </tr>
              <tr>
              <td> A set of near field emission measurements has been be performed in order to evaluate shield's effectiveness. The shields were mounted on a PCC card from MTD BTL project. Please visit <a href="pcc_news.php">PCC news feed</a> for more details.</td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-03-01">2022-03-01</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A new company entered the competition for the mass production of the LVR's toroids. An interesting quotatation and a proof of concept prototype have been both presented to ETH. We have decided to place an order for a batch of 650 pieces in parallel with the previous round's winner. This is to compare both products and have a better understanding where to place an order for the final 23,000 pieces production.</td>
              </tr>
              <tr>
                <td><img src="pictures/ita_toroid.jpg" alt="ITA toroid" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

        <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-28">2022-02-28</a></th>
              </tr>
            </thead>
            <tbody >             
              <tr>
                <td>A dummy LVR v3.1 card has been wired up. The card is intended for full trigger tower testing including the newest FE card from University of Notre Dame US. Currently the card is undergoing test in bat. 892 and will be delivered to UND colleagues later this week.</td>
              </tr>
              <tr>
                <td><img src="pictures/lvr_dummy.png" alt="LVR dummy" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-25">2022-02-25</a></th>
              </tr>
            </thead>
            <tbody>             
              <tr>
                <td>Metal housings have been produced for LVR v 3.1 cards and successfully integrated. The test in a trigger tower showed a perfect alignment of respective pins and screws.</td>
              </tr>
              <tr>
                <td><img src="pictures/lvr_housing1.jpg" alt="LVR housing1" width="50%" height="50%" align="center">
                <img src="pictures/lvr_housing2.jpg" alt="LVR housing2" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
          </table><br />

          <table width="50%" align="center" border ="1">
            <thead>
              <tr>
                <th align="center"><a id="2022-02-23">2022-02-23</a></th>
              </tr>
            </thead>
            <tbody>             
              <tr>
                <td>4 panels of 2 (8 PCBs) LVR v3.1 cards arrived to CERN</td>
              </tr>
              <tr>
                <td><img src="pictures/lvr_panel.JPG" alt="LVR panel" width="50%" height="50%" align="center"></td>
              </tr>
            </tbody>
        </table><br />

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
