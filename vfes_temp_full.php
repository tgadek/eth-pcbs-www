<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ETH Zurich Printed Circuit Boards Data Base Viewer</title>
    <link rel = "icon" href = "logo_mini.png" type = "image/x-icon">
    <style type="text/css">
    info_style {text-align: right; font-family: Helvetica, sans-serif; font-size: 15px;}
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    table {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    tr {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    td {text-align: center; font-family: Currier New, monospaced; font-size:12px;}
    th {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    p {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    div {text-align: center; font-family: Helvetica, sans-serif; font-size: 30px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    img { max-width: 100%; height: auto; }
    </style>
</head>
<body>
<img position="absolute" src="logo.png" alt="ETH Logo" width="400" height="70" align="right">
<= <a href="index.php">BACK to HOMEPAGE</a>
    <h1>
        <br />
        <table width="50%" align="center">
            <thead>
              <tr>
                <th>VFEs</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src="vfe.PNG" alt="VFE picture" width="20%" height="20%" align="center"></td>
              </tr>
            </tbody>
        </table>
    </h1>
    <p align="center">
    You are displaying a full set of test outcomes waiting for approval. To view a reduced set of data please switch to 
    <a href="vfes_temp_results.php">VFE reduced info [approval pending]</a>. <br /> To view test overview screenshots please click 
    <a href="vfes_temp_screenshots.php">VFE screenshots [approval pending]</a>. <br /> To view approved test data please switch to 
    <a href="vfes_full.php">VFE test results</a>.
</p>
<br/>

<?php

    $host    = "dbod-vfe-test-results.cern.ch:5506";
    $user    = "website";
    $pass    = "website_VFEs";
    $db_name = "vfe_temp_test_results";

    //create connection
    //mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $connection = mysqli_connect($host, $user, $pass, $db_name);
    // Check connection
    if (mysqli_connect_errno())
    {
        echo '<status_error align="center"> Failed to connect to MySQL: ' . mysqli_connect_error(); 
        echo '<br/></status_error>';
    }
    else
    {
        //get results from database
        $result = mysqli_query($connection, "SELECT vfe_id,
vfe_name,
responsible_name,
responsible_surname,
location,
date,
status,
test_number,
i2c_errors,
ch1_g1_mean,
ch1_g1_rms,
ch2_g1_mean,
ch2_g1_rms,
ch3_g1_mean,
ch3_g1_rms,
ch4_g1_mean,
ch4_g1_rms,
ch5_g1_mean,
ch5_g1_rms,
ch1_g10_mean,
ch1_g10_rms,
ch2_g10_mean,
ch2_g10_rms,
ch3_g10_mean,
ch3_g10_rms,
ch4_g10_mean,
ch4_g10_rms,
ch5_g10_mean,
ch5_g10_rms,
yml_file,
# run_data_txt,
# ext_lin_pulse_data_txt,
# int_lin_pulse_data_txt,
# ext_single_pulse_event_txt,
# int_single_pulse_event_txt,
ch1_temperature,
ch2_temperature,
ch3_temperature,
ch4_temperature,
ch5_temperature,
fpga_temperature,
ch1_g1_inl_int,
ch1_g1_inl_ext,
ch2_g1_inl_int,
ch2_g1_inl_ext,
ch3_g1_inl_int,
ch3_g1_inl_ext,
ch4_g1_inl_int,
ch4_g1_inl_ext,
ch5_g1_inl_int,
ch5_g1_inl_ext,
ch1_g10_inl_int,
ch1_g10_inl_ext,
ch2_g10_inl_int,
ch2_g10_inl_ext,
ch3_g10_inl_int,
ch3_g10_inl_ext,
ch4_g10_inl_int,
ch4_g10_inl_ext,
ch5_g10_inl_int,
ch5_g10_inl_ext,
1v2_current,
2v5_current,
ped_scan_params
FROM results ORDER BY vfe_id");
        $all_property = array();  //declare an array for saving property

        //showing property
        echo '<table border = "1" align = "center">
                <tr >';  //initialize table tag
        while ($property = mysqli_fetch_field($result)) {
            echo '<td border="1">' . $property->name . '</td>';  //get field name for header
            $all_property[] = $property->name;  //save those to array
        }
        echo '</tr>'; //end tr tag

        //showing all data
        while ($row = mysqli_fetch_array($result)) {
            echo "<tr>";
            foreach ($all_property as $item) {
                echo '<td border="1">' . $row[$item] . '</td>'; //get items using property value
            }
            echo '</tr>';
        }
        echo "</table>";
    }
?>

</body>
<footer align = "right">
    <foot> <br /> powered by Tomasz Gadek & coffee </foot>
</footer>
</html>
